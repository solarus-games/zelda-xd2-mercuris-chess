#!/bin/sh
# Install script for zelda-xd2-mercuris-chess.

# Copyright 2017-2020 orbea
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# IFS is <space><tab><newline>
IFS=' 	
'
\unset -f command printf unalias : 2>/dev/null
\unalias -a 2>/dev/null
PATH="$(command -p getconf PATH):$PATH"
LC_ALL=C; export LC_ALL

set -euf

die () {
  ret="$1"; shift
  case "$ret" in
    : ) printf %s\\n "$@" >&2; return 0 ;;
    0 ) printf %s\\n "$@" ;;
    * ) printf %s\\n "$@" >&2 ;;
  esac
  exit "$ret"
}

case "${1:-}" in
  install-assets ) INSTALL=4 ;;
  clean ) INSTALL=3 ;;
  build ) INSTALL=2 ;;
  install ) INSTALL=1 ;;
  uninstall ) INSTALL=0 ;;
  '' ) die 1 'No install target.' ;;
  * ) die 1 "No rule to install target '$1'." ;;
esac

cmd () {
  print=
  for command do
    case "$command" in
      -- ) : ;;
      */* ) print="${print} '$command'" ;;
      * ) print="${print} $command" ;;
    esac
  done
  printf " %s\\n" "$print"
  command -p "$@"
}

is_absolute () { case "$1" in /*) return 0 ;; *) return 1 ;; esac; }

PREFIX=; BINDIR=; DATADIR=; DATANAME=; DATAROOTDIR=; DOCDIR=; MANDIR=
SYSCONFDIR=; DESTDIR=

CWD="$(pwd)"

if [ "$0" != "${0%/*}" ]; then
  SOURCE_DIR="$(cd -- "${0%/*}/" && pwd -P)"
else
  SOURCE_DIR="$(pwd -P)"
fi

quest_version="$(grep quest_version "$SOURCE_DIR"/data/quest.dat)"
quest_version="${quest_version%\"*}"

PRGNAM=zelda-xd2-mercuris-chess
VERSION="${quest_version#*\"}"

shift

install_args=

while [ $# -gt 0 ]; do
  arg="$1"; shift
  case "${arg%%=*}" in
    DATANAME|DESTDIR|PREFIX )
      eval "${arg%%=*}=\"\${arg#*=}\""
    ;;
    BINDIR|DATADIR|DATAROOTDIR|DOCDIR|MANDIR|SYSCONFDIR )
      install_args=1
      eval "${arg%%=*}=\"\${arg#*=}\""
    ;;
    * )
      die : "WARNING: Variable '${arg%%=*}' was not used by $PRGNAM."
    ;;
  esac
done

PREFIX="${PREFIX:-/usr/local}"
BINDIR="${BINDIR:-${PREFIX}/bin}"
DOCDIR="${DOCDIR:-${PREFIX}/share/doc}/$PRGNAM-$VERSION"
MANDIR="${MANDIR:-${PREFIX}/share/man}"
DATANAME="${DATANAME:-data.solarus}"
DATAROOTDIR="${DATAROOTDIR:-${PREFIX}/share}"
DATADIR="${DATADIR:-${DATAROOTDIR}}/solarus/$PRGNAM"
SYSCONFDIR="${SYSCONFDIR:-${PREFIX}/etc}"
DESTDIR="${DESTDIR:-}"

is_absolute "$PREFIX" || PREFIX="$CWD/$PREFIX"

if [ -n "$install_args" ]; then
  for i in \$BINDIR \$DATADIR \$DATAROOTDIR \$DOCDIR \$MANDIR \$SYSCONFDIR; do
    eval "dest=\"$i\""
    is_absolute "${dest:?}" || eval "${i#*$}=\"\${PREFIX}/$i\""
  done
fi

####################
### Project Code ###
####################

build_bin () {
  command -p rm -f -- "$CWD/$PRGNAM"
  printf 'Generating %s script...\n' "$PRGNAM"
  printf '#!/bin/sh\nsolarus-run %s "$@"\n' "$DATADIR" > "$CWD/$PRGNAM"
}

build_data () {
  command -p rm -f -- "$CWD/$DATANAME"
  printf 'Generating %s...\n' "$DATANAME"
  (
    cd -- "${SOURCE_DIR}"/data/
    command -p find . -type f \( \
      -name '*.spc' -o \
      -name '*.ogg' -o \
      -name '*.it'  -o \
      -name '*.png' -o \
      -name '*.dat' -o \
      -name '*.lua' -o \
      -name '*.ttf' -o \
      -name '*.ttc' -o \
      -name '*.fon' -o \
      -name '*.glsl'   \
    \) -exec zip -9 -q "$CWD/$DATANAME" {} +
  )
}

icon_sizes='16 20 22 32 40 48 64 96 128 256 512 1024'

case $INSTALL in
  # install-assets
  4 )
    printf 'Installing assets...\n'
    cmd mkdir -p -- "${DESTDIR}${DATAROOTDIR}/applications"
    cmd mkdir -p -- "${DESTDIR}${DATAROOTDIR}/pixmaps"
    cmd mkdir -p -- "${DESTDIR}${DOCDIR}"
    cmd rm -f -- "${DESTDIR}${DOCDIR}/changelog.md"
    cmd cp -- "${SOURCE_DIR}/changelog.md" "${DESTDIR}${DOCDIR}"
    cmd rm -f -- "${DESTDIR}${DOCDIR}/license.txt"
    cmd cp -- "${SOURCE_DIR}/license.txt" "${DESTDIR}${DOCDIR}"
    cmd rm -f -- "${DESTDIR}${DOCDIR}/readme.md"
    cmd cp -- "${SOURCE_DIR}/readme.md" "${DESTDIR}${DOCDIR}"
    cmd rm -f -- "${DESTDIR}${DATAROOTDIR}/applications/$PRGNAM.desktop"
    cmd cp -- "${SOURCE_DIR}/$PRGNAM.desktop" \
      "${DESTDIR}${DATAROOTDIR}/applications"
    cmd rm -f -- "${DESTDIR}${DATAROOTDIR}/pixmaps/$PRGNAM.png"
    cmd cp -- "${SOURCE_DIR}/data/logos/logo_2x.png" \
      "${DESTDIR}${DATAROOTDIR}/pixmaps/$PRGNAM.png"

    eval "set -- $icon_sizes"
    for icon do
      icon_dir="${DESTDIR}${DATAROOTDIR}/icons/hicolor/${icon}x${icon}/apps"
      cmd mkdir -p -- "$icon_dir"
      cmd rm -f -- "$icon_dir/$PRGNAM.png"
      cmd cp -- "${SOURCE_DIR}/data/logos/icon_$icon.png" \
        "$icon_dir/$PRGNAM.png"
    done
  ;;
  # clean
  3 )
    printf 'Removing %s...\n' "$DATANAME"
    cmd rm -f -- "$CWD/$DATANAME"
    cmd rm -f -- "$CWD/$PRGNAM"
  ;;
  # build
  2 )
    build_data
    build_bin
  ;;
  # install
  1 )
    [ -f "$CWD/$DATANAME" ] || build_data
    [ -f "$CWD/$PRGNAM" ] || build_bin
    printf 'Installing...\n'
    cmd mkdir -p -- "${DESTDIR}${BINDIR}"
    cmd mkdir -p -- "${DESTDIR}${DATADIR}"
    cmd rm -f -- "${DESTDIR}${BINDIR}/$PRGNAM"
    cmd cp -- "$CWD/$PRGNAM" "${DESTDIR}${BINDIR}"
    cmd rm -f -- "${DESTDIR}${DATADIR}/$DATANAME"
    cmd cp -- "$CWD/$DATANAME" "${DESTDIR}${DATADIR}"
    cmd chmod 0755 -- "${DESTDIR}${BINDIR}/$PRGNAM"
  ;;
  # uninstall
  0 )
    printf 'Uninstalling...\n'
    cmd rm -rf -- "${DESTDIR}${DOCDIR}"
    cmd rm -rf -- "${DESTDIR}${DATADIR}"
    cmd rm -f -- "${DESTDIR}${BINDIR}/$PRGNAM"
    cmd rm -f -- "${DESTDIR}${DATAROOTDIR}/applications/$PRGNAM.desktop"
    cmd rm -f -- "${DESTDIR}${DATAROOTDIR}/pixmaps/$PRGNAM.png"

    eval "set -- $icon_sizes"
    for icon do
      icon_dir="${DESTDIR}${DATAROOTDIR}/icons/hicolor/${icon}x${icon}/apps"
      cmd rm -f -- "$icon_dir/$PRGNAM.png"
    done
  ;;
esac

exit 0
